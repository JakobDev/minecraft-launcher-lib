<!-- Before reporting a Bug, please read https://minecraft-launcher-lib.readthedocs.io/en/latest/faq.html and https://minecraft-launcher-lib.https://minecraft-launcher-lib.readthedocs.io/en/latest/troubleshooting.html first -->
### Summary

<!-- Please describe the Bug here -->

### Code
<!-- Paste the codes that causes the Bug here. It should be possible the reproduce the Bug with this code. -->
```python
Replace this text with your code
```

### Error
<!-- If minecraft-launcher-lib crashes with a error message/traceback, paste it here. If not, please remove this section. -->
```
Replace this text with your error
```

### System
Operating system: <your os>  
Python version:  <your python version>  

### Minecraft version
<!--  If your Bug belongs to a specific Minecraft version, paste the exact version id here. If not, remove this section. If this is a nodded version, please provide the download link. -->

/label ~bug
