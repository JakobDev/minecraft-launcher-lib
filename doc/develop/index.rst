Develop
==================================================

.. toctree::
    :maxdepth: 2

    testing_changes
    codestyle
    automatic_tests
    build_and_edit_documentation
    making_a_merge_request
