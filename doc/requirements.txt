sphinx==5.1.1
sphinx-rtd-theme==1.0.0
sphinx-reredirects==0.0.1
sphinx-notfound-page==0.8
sphinx-rtd-dark-mode==1.2.4
sphinx-copybutton==0.5.0
