Getting Help
==========================
Here are some sources if you need more information.

- This documentation contains under modules a list of modules with every function you can use. You should check it out. You can test the functions in a interactive Python Shell.
- Check out the `Examples on GitLab <https://gitlab.com/JakobDev/minecraft-launcher-lib/-/tree/master/examples>`_.
- You can check out the source of the programs in the :doc:`/showcase`.
- If none of them above help, feel free to `open a Issue <https://gitlab.com/JakobDev/minecraft-launcher-lib/-/issues>`_.
