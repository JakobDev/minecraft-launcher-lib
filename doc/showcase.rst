Showcase
==========================
Here are a list of programs that uses minecraft-launcher-lib. If you want to add yours, feel free to make a MR on GitLab.

- `jdMinecraftLauncher <https://gitlab.com/JakobDev/jdMinecraftLauncher>`_
- `minecraft-launcher-cmd <https://gitlab.com/JakobDev/minecraft-launcher-cmd>`_